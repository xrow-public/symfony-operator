# How to use Blackfire

## Modify the Developer CRD

Copy your [credendials](https://blackfire.io/my/settings/credentials). 

```yaml
apiVersion: xrow.com/v1
kind: Developer
....
  env:
  - name: BLACKFIRE_CLIENT_ID
    value: XXX
  - name: BLACKFIRE_CLIENT_TOKEN
    value: XXX
  - name: BLACKFIRE_SERVER_ID
    value: XXX
  - name: BLACKFIRE_SERVER_TOKEN
    value: XXX
...
```

## Run some requests

You need to copy the cookie from your browser into the env frist.

```bash
COOKIE="eZSESSID21232f297a57a5a743894a0e4a801fc3=6a496ea21b98c357ff1e7c0cc0082a57"

blackfire client:curl \
  -H "x-siteaccess: admin" \
  -H "Cookie: ${COOKIE}" \
  http://127.0.0.1:8080/dashboard
```

```bash
COOKIE="eZSESSID21232f297a57a5a743894a0e4a801fc3=6a496ea21b98c357ff1e7c0cc0082a57"

blackfire client:curl \
  -H "x-siteaccess: admin" \
  -H "Cookie: ${COOKIE}" \
  -H "accept: application/vnd.ez.api.View+json"  \
  -H "content-type: application/vnd.ez.api.ViewInput+json;version=1.1" \
  --data-raw '{"ViewInput":{"identifier":"navigation","public":false,"LocationQuery":{"FacetBuilders":{},"Filter":{"ContentTypeIdentifierCriterion":"page","SubtreeCriterion":"/1/2/","VisibilityCriterion":false},"SortClauses":{"LocationDepth":"ascending"},"limit":300,"offset":0}}}' \
  "http://127.0.0.1:8080/api/ezp/v2/views" \
  --compressed
```

Visit the [Blackfire dashboard](https://blackfire.io/my/profiles).

