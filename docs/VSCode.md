# Working with **VS Code**

This guide helps you connet your recommended IDE [VS Code](https://code.visualstudio.com/).

## Setup the IDE [VS Code](https://code.visualstudio.com/)

* Install the IDE [VS Code](https://code.visualstudio.com/)
* Install the [kubernetes extension](https://marketplace.visualstudio.com/items?itemName=ms-kubernetes-tools.vscode-kubernetes-tools) in [VS Code](https://code.visualstudio.com/)
* Install the [remote SSH extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh) in [VS Code](https://code.visualstudio.com/)
* Aquire a [kubeconfig](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/) from an administrator from a service account with sufficent rights to connect to the api.
* Place the [kubeconfig](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/) in the standard kubernetes directory ~/.kube/config
* Use the kubernetes extension, select the proper [kubeconfig](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/) and connect to kubernetes via the extension. Errors fetching namespaces might be desired from the administrator.
* Create a [Developer CRD](DeveloperWorkspace.md) wiht your public key credentials
* Navigate to your developer pod created by the [Developer CRD](DeveloperWorkspace.md) under workloads and right click to forward all ports
* Connect via remote SSH wiht this recommended settings:

**`~/.ssh/ssh_config`**
```yaml
Host "Development Pod"
  HostName 127.0.0.1
  User root
  Port 2222
  ForwardAgent yes
  IdentityFile ~/.ssh/id_ed25519
  StrictHostKeyChecking no
  UserKnownHostsFile /dev/null
  # ProxyJump vagrant
```

* If you are connecting over a vs code remote shell, you can use the ProxyJump setting from above.
* If you are connected, open the projects root folder
* **You are now ready to start coding.** You can switch to the master branch and create for example a feature branch.
* You can access your webserver via http://127.0.0.1:8080 or https://127.0.0.1:8443

In case you use vs code via a remote host you can make kubectl port-forward listen to all interfaces via alias in your profile (e.g. /etc/profile.d/vscode.sh). 

**`/etc/profile.d/vscode.sh`**
```bash
# Make vs code listen too all interfaces
kubectl() {
    if [ "$1" = "port-forward" ]
    then
        vars=${@:2}
        echo "Alias kubectl port-forward --address 0.0.0.0 $vars"
        command kubectl port-forward --address 0.0.0.0 $vars
    else
        command kubectl $@
    fi;
}
```

If you don`t use a ForwardAgent SSH agent you can add the key to development pod

1. Add a new connection
2. Place your key in `~/.ssh/id_ed25519` or create a new key with `ssh-keygen -t ed25519`
3. `chmod 600 ~/.ssh/id_ed25519`

## Guide to create a kubeconfig from the default service account 

This is not fully working or incomplete. We keep this part for future reference. 

```bash
#!/bin/bash
mkdir -p ~/.kube
chmod 600 ~/.kube

# Point to the internal API server hostname
APISERVER=https://kubernetes.default.svc
# Path to ServiceAccount token
SERVICEACCOUNT=/var/run/secrets/kubernetes.io/serviceaccount
# Read this Pod's namespace
NAMESPACE=$(cat ${SERVICEACCOUNT}/namespace)
# Read the ServiceAccount bearer token
TOKEN=$(cat ${SERVICEACCOUNT}/token)
# Reference the internal certificate authority (CA)
CACERT=${SERVICEACCOUNT}/ca.crt
# Explore the API with TOKEN
curl --cacert ${CACERT} --header "Authorization: Bearer ${TOKEN}" -X GET ${APISERVER}/api

TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
NAMESPACE=$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace)
cat << EOF > ~/.kube/config
apiVersion: v1
clusters:
- cluster:
    server: https://kubernetes.default.svc:8443
  name: local
contexts:
- context:
    cluster: local
    namespace: $NAMESPACE
    user: namespace-token-user
  name: default
current-context: default
kind: Config
preferences: {}
users:
- name: namespace-token-user
  user:
    token: $TOKEN
EOF
chmod 600 ~/.kube/config
```