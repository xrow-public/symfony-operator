# The Developer Workspace

## Prerequisites

* A running ibexa installation set up via the [ibexa operator](https://ibexa-operator.xrow.de) or via the [ibexa helm chart](https://ibexa-operator.xrow.de).

## Workspace Setup

Take the sample YAML configuration and modify it:

* Look up the name of the PVC (persistent volume claim) used as the file storage of `ibexa` and add it to use CRD as "claimName" under `spec`
* Look up the ConfigMap name of the PVC (persistent volume claim) and add it to use CRD as "configMap" under `spec`
* Enter ip, key (ssh public key), email (your gitlab email), name (your full name) and the desired ezplatform docker image and repository credentials

### Sample YAML to create a developer workspace

```yaml
apiVersion: xrow.com/v1
kind: Developer
metadata:
  name: developer01
  namespace: ibexa-test
spec:
  release: ibexa
  key: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOXGvbBWWfPKoMEU8zKMaKGLocGLVMAq4K24UZMyzjmc"
  email: "bjoern@xrow.de"
  name: "Björn Dieding"
  image: 
    registry: registry.gitlab.com
    repository: xrow-shared/helm-ezplatform/ibexa-oss/dev
    tag: latest
  env:
    - name: APP_ENV
      value: dev
    - name: APP_DEBUG
      value: 1
```

## Create the CRD

* Save your customized YAML configuration somewhere. Keep it for future reuses!
* Open the OpenShift _Application Console_ and navigate to the Namespace of your Project
* Insert the YAML by using **Add to Project** and **Import YAML / JSON**

*This will create a CRD, type "developer", which generates a new Pod (as a development instance) in the Namespace (with the name you have specified in _metadata/name_)*

## Port Forward (optional)

This action is needed if you lag the VPN access to the cluster

```bash
kubectl config use-context 06.xrow.net
kubectl config set-context --current --namespace=project-test 
kubectl port-forward service/bjoern-developer 2222:2222 &
kubectl port-forward service/bjoern-developer 8080:8080 &
kubectl port-forward service/bjoern-developer 8443:8443 &
kubectl port-forward service/$(kubectl get service | grep solr | awk '{print $1}') 8983:8983 &
kubectl port-forward service/$(kubectl get service | grep mysql | awk '{print $1}') 3306:3306 &
```

Alternativly you can forward with a desktop app like [Lens](https://k8slens.dev/desktop.html)

![Lens](images/lens.png)

## Connect to the Pod / development environment via an SSH Agent

### Linux: Connect via SSH

```
[root@host ~]# ssh-add 
Identity added: /root/.ssh/id_rsa (/root/.ssh/id_rsa)
[root@host ~]# ssh-add -l
2048 SHA256:KzftMoyTmCcuJ+Yt6mMs/piqbNP5H5M2dplu/+FHF/E /root/.ssh/id_rsa (RSA)
[root@host ~]# ssh default@10.10.7.10 -p 2222 -A
Last login: Tue Feb 18 00:37:33 2020 from 10.128.0.1
Hi Björn Dieding <bjoern@xrow.de>, you can start coding
-bash-4.2$ ssh -T git@gitlab.com
Warning: Permanently added 'gitlab.com,35.231.145.151' (ECDSA) to the list of known hosts.
Welcome to GitLab, @xrow!
```


### Windows: Connect via SSH

1. Open a **PowerShell** as Administrator and start SSH Agent

```powershell
$OpenSSHClient = Get-WindowsCapability -Online | ? Name -like ‘OpenSSH.Client*’
Add-WindowsCapability -Online -Name $OpenSSHClient.Name
Set-Service ssh-agent -StartupType Automatic
Start-Service ssh-agent
Get-Service ssh-agent
```

2. Temporary Bugfix (https://github.com/PowerShell/Win32-OpenSSH/issues/1234)
```powershell
sc.exe create sshd binPath=C:\Windows\System32\OpenSSH\ssh.exe
```

1. Open a **PowerShell** as Administrator and add your users ssh key

```powershell
PS C:\Users\bjoern\.ssh> C:\Windows\System32\OpenSSH\ssh-add.exe $env:userprofile\.ssh\id_rsa
Identity added: id_rsa (id_rsa)
```

2. In your user OS profile place a _.ssh/config_ with the proper contents

```
Host 192.168.3.27
  HostName 192.168.3.27
  User default
  Port 2223
Host *
  StrictHostKeyChecking no
  ForwardAgent yes
  UserKnownHostsFile=/dev/null
```

### Set up alternative SSH Agent 

https://github.com/benpye/wsl-ssh-pageant

start D:\bin\wsl-ssh-pageant-amd64-gui.exe -systray -verbose -winssh ssh-pageant

## Debug with VS Code and Xdebug

1. In **VS Code** install [PHP Debug Extension](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug)
2. In **Chrome** install [Xdebug helper](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc)
3. Add your developer CRD in the namespace where ibexa is running
4. Add the launch.json
```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Listen for XDebug",
            "type": "php",
            "request": "launch",
            "port": 9000,
            "stopOnEntry": true,
            "pathMappings": {
                "/opt/app-root/src": "${workspaceFolder}/"
            },
            "ignore": [
                "**/vendor/**/ContainerConfigResolver.php",
                "**/bootstrap.php",
                "**/vendor/**/SiteAccessConfigResolver.php"
            ]
        },
        {
            "name": "Launch currently open script",
            "type": "php",
            "request": "launch",
            "program": "${file}",
            "cwd": "${fileDirname}",
            "port": 9000
        }
    ]
}
```
5. Connect to the remote developer pod
6. Click "Run" on the left-hand side in **VS Code**
7. Clink "Listen for XDebug" in the top menu of **VS Code**
8. Open the page in the browser **VS Code** http://<extermal ip>:8080 or http://<kubernetes node ip>:<node port from developer pod>
7. Start debugging by opening a page.
