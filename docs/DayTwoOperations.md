# Day two operations


## Setting an ENV variable

You can set an ENV variable for different purposes in different ways. By default, helm, kubernetes and ConfigMap variables 
for services are injected by default.

View them by typing `env`.

If you want to set a static env variable, set them via .s2i/environment. It might be better though to set a static symfony parameter.
If you want to set variables that differ per stage, set them in the helm chart or via ezplatform / developer operator.

```yaml
  env:
  - name: APP_ENV
    value: "dev"
  - name: APP_DEBUG
    value: "1"
```

# Recreate all dev pods

To force the regenaration of all pods you can follow this logic.

```bash
oc get Developer --all-namespaces
oc get Developer --all-namespaces -o yaml > export.yaml
oc get developer --no-headers=true --all-namespaces | sed -r 's/(\S+)\s+(\S+).*/oc --namespace \1 delete developer \2/e'
oc apply -f export.yaml
```
