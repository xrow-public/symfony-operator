# Troubleshooting

## Image Pull Backoff

This could have the following reasons:

* You are notified that your images repositories credentials are incorrect or the image doen`t exists.
* Your image is very big.
* You use persistent storage for your code. Which slows down the inital startup. 

## Image tag latest gives 500

* Try to find an older version of that image that works.

## Your pod returns a 500 errors / Using an image with code errors

Things to consider:

* latest image is broken
* try a different image
* your code might be broken, try to fix the code 
* the data in the database might be awkward and might cause the application to fail
* binary data might be missing that causes an exception in the code

## Your POD IP is not reachable

* You might be running two developer CRCs that use the same external IP

## The result you see in the browser is not the result you expect

* You have been using your OS hosts file to address the POD, meanwhile you changed the IP of the POD and your hosts file, but browser doesn`t notice. Restart the browser.

## Can`t login via ssh

Error: child_set_env: too many env vars

https://github.com/brl/obfuscated-openssh/blob/master/session.c#L972

This can be fixed by removing the amount of pods in a namespace.

## Dev the system is slow in general

Here are hint on how fast a System should be

| Scenario | Result |
| --- | --- |
| APP_ENV=prod, XDEBUG_MODE=, BLACKFIRE-*= | Request will take 1 sec it has no overhead |
| APP_ENV=dev,APP_DEV=1, XDEBUG_MODE=, BLACKFIRE-*= | Request will take 3 sec it has overhead of the webdeveloper toolbar |
| APP_ENV=dev,APP_DEV=1, XDEBUG_MODE=develop, BLACKFIRE-*= | Request will take 6, if the IDE is connected. **It will take 60+ sec if the IDE is not listending to the request, because XDEBUG can`t reach the IDE (timeout). See /tmp/xdebug.log** |
| APP_ENV=prod,APP_DEV=,XDEBUG_MODE=, BLACKFIRE-*=XXX | Request will take 4 sec, has overhead of the Blackfire runnign in the background |

## If you use the env APP_DEV set to dev the system is slow

* Check the logs. There might be to many unseen errors.

## If you use the env XDEBUG_MODE set to dev the system is slow

* Check /tmp/xdebug.log for connection errors, this mean you have not startet the remote debugging
* If you do not need to debug pph code remove XDEBUG_MODE from env 

## You see an out of memory error

* You might have var_dump in the code.