var consent = __md_get("__consent")
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
if ( urlParams.get('gtm_debug') || consent && consent.analytics) {
  console.log("Google Tag Manager");

  // Google Tag Manager
  (function (w, d, s, l, i) {
    w[l] = w[l] || []; w[l].push({
      'gtm.start':
        new Date().getTime(), event: 'gtm.js'
    }); var f = d.getElementsByTagName(s)[0],
      j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
  })(window, document, 'script', 'dataLayer', 'GTM-NN5D7RC');
}