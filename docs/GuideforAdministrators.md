# Guide for Administrators

## Grant Access via a auth provider

* [Grant a Gitlab User Access](https://docs.openshift.com/container-platform/3.11/install_config/configuring_authentication.html#LookupMappingMethod)

[Setup a new Application Token](https://gitlab.com/-/profile/applications)

Add the gitlab auth provider

```yaml
oauthConfig:
  assetPublicURL: https://openshift.XXX.xrow.net:8443/console/
  grantConfig:
    method: auto
  identityProviders:
  - challenge: true
    login: true
    mappingMethod: lookup
    name: gitlab
    provider:
      apiVersion: v1
      clientID: XXX
      clientSecret: XXX
      kind: GitLabIdentityProvider
      legacy: false
      url: https://gitlab.com/
```

Restart Controller

For Openshift 3.11:

```bash
master-restart api
master-restart controllers
```

## Map an identity 


### Create a cluster admin user

```bash
oc create identity gitlab:44745
oc create useridentitymapping gitlab:44745 admin
```

### Create a namespace admin

```bash
oc create identity gitlab:44745
oc create user service@xrow.de --full-name="xrow GmbH"
oc policy add-role-to-user admin service@xrow.de -n project-name-production
oc create useridentitymapping gitlab:44745 service@xrow.de
```

### Create a namespace developer:

Uses the special role developer-operator-developer.

```bash
oc create identity gitlab:44745
oc create user service@xrow.de --full-name="xrow GmbH"
oc policy add-role-to-user developer-operator-developer service@xrow.de -n project-name-production
oc create useridentitymapping gitlab:44745 service@xrow.de
```

### Revoke access

```bash
oc delete useridentitymapping gitlab:44745
```