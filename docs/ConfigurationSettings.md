# Configuration settings

## Common settings


| Configuration | Default | Description | Sample |
| --- | --- | --- | --- |
| env | {} | Env Variables | - |
| ip | null | Optional remote IP | 192.168.4.2 |
| image.repository| registry.gitlab.com/xrow-shared/ezplatform/dev | image to use | - |
| image.tag | latest | Use hash or default | - |
| image.pullPolicy | Always | - | - |
| imageCredentials.registry | - |  Not required since the service account has access  | registry.gitlab.com |
| imageCredentials.username | - | Not required since the service account has access | gitlab+deploy-token-183447 |
| imageCredentials.password | - | Not required since the service account has access | ABCDEF1234 |
| storage.enabled | true | enable or disable persistance for your code | - |
| storage.class | - | set a custom storage class | - |
| storage.mount | /opt/app-root/src/public/var | set the nfs mount point for your project | - |
| claimName | - | Name of the mounted claim wiht data  | - |
| configMap | - | Name of the config map with env settings | - |

## PHP Symfony

See https://gitlab.com/xrow-public/docker-php-ezplatform

```yaml
apiVersion: xrow.com/v1
kind: Developer
....
  env:
    - name: APP_ENV
      value: dev
    - name: APP_DEBUG
      value: 1
...
```

## Node

See https://gitlab.com/xrow-public/s2i-nodejs-container

```yaml
apiVersion: xrow.com/v1
kind: Developer
....
  env:
    - name: NODE_ENV
      value: development
    - name: DEV_MODE
      value: 'true'
...
```
