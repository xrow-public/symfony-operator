
[PDF Download](https://xrow-shared.gitlab.io/developer-operator/pdf/document.pdf)

# Installation

## Helm chart installation

The installation step needs to be done only once per cluster. You may skip this step if you have a cluster already enabled.

Install stable:

```yaml
helm upgrade --install symfony-operator-crd oci://registry.gitlab.com/xrow-public/symfony-operator/symfony-operator-crd --version {{ scm.latest }} -n kube-system
```

```yaml
helm upgrade --install symfony-operator oci://registry.gitlab.com/xrow-public/symfony-operator/symfony-operator --version {{ scm.latest }} -n kube-system
```

Install latest:

```yaml
helm upgrade --install symfony-operator-crd oci://registry.gitlab.com/xrow-shared/symfony-operator/charts/symfony-operator-crd --version 0.0.0+{{ git.short_commit}} -n kube-system
```

```yaml
helm upgrade --install symfony-operator oci://registry.gitlab.com/xrow-shared/symfony-operator/charts/symfony-operator --version 0.0.0+{{ git.short_commit}} -n kube-system
```

```txt
{% if git.status %}The latest unstable build is version 0.0.0+{{ git.short_commit}} from {{ git.date}} by {{ git.author }}{% endif %}
```

## Helm chart uninstall

```yaml
kubectl delete symfony --all-namespaces --all
helm uninstall symfony-operator -n kube-system
kubectl delete crd symfony.xrow.com
```
