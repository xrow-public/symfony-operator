package licence

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"

	"github.com/go-logr/logr"
	"github.com/operator-framework/helm-operator-plugins/pkg/hook"
	chart "helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chartutil"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	ctrl "sigs.k8s.io/controller-runtime"
	client "sigs.k8s.io/controller-runtime/pkg/client"
)

var (
	API          = "https://virtserver.swaggerhub.com/XROW-GMBH/licence/0.0.0"
	log          = ctrl.Log.WithName("licence")
	trialLicence = Licence{
		Active: 0,
		Limit:  3,
	}
)

type Licence struct {
	Limit  int `json:"limit"`
	Active int `json:"active"`
}

type LicenceManager struct {
	Licence Licence
	chart   *chart.Chart
	mgr     ctrl.Manager
}

func NewLicenceManager(mgr ctrl.Manager, ch *chart.Chart) *LicenceManager {
	l := new(LicenceManager)
	l.chart = ch
	l.mgr = mgr
	l.Licence = l.GetLicence()
	return l
}
func (l *LicenceManager) GetHook() (h hook.PreHook) {
	lmgr := l
	h = hook.PreHookFunc(func(obj *unstructured.Unstructured, values chartutil.Values, log logr.Logger) (err error) {
		key := client.ObjectKeyFromObject(obj)
		gvk := obj.GroupVersionKind()
		fmt.Println(gvk)
		//var ctx context.Context
		//
		// var ctx context.Context // For debugging; delete when done.
		ctx := context.TODO()
		//ctx := lmgr.mgr.data.internalCtx
		cl := lmgr.mgr.GetClient()
		fmt.Println(cl)

		fmt.Println(obj.GetUID())
		//private fmt.Println(lmgr.mgr.cluster.Cluster.GetConfig().Host)
		fmt.Println(ctx)
		fmt.Println(key)
		obj2 := &unstructured.Unstructured{}
		obj2.SetGroupVersionKind(gvk)
		//cl.Get(context.Background(), obj)

		// key := client.ObjectKeyFromObject(obj)

		err = cl.Get(ctx, key, obj2)
		fmt.Println(obj2)
		// @TODO bjoern
		// _ = cl.Delete(ctx, obj2)

		// Since the client is hitting a cache, waiting for the
		// deletion here will guarantee that the next reconciliation
		// will see that the CR has been deleted and that there's
		// nothing left to do.
		//if err := controllerutil.WaitForDeletion(ctx, client, obj); err != nil {
		//	return err
		//}
		err = errors.New("math: square root of negative number" + strconv.Itoa(lmgr.Licence.Limit))
		return err
	})
	return h
}
func (l *LicenceManager) GetLicence() (licence Licence) {
	client := &http.Client{}

	req, err := http.NewRequest("POST", API+"/licence/"+l.chart.Metadata.Name, nil)
	req.Header.Add("Authorization", os.Getenv("OPERATOR_LICENCE_KEY"))
	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)

	if err != nil {
		log.Error(err, "Server not reachable. Using trail licence.")
		return trialLicence
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error(err, "Server response invalid or licence not valid. Using trail licence.")
		return trialLicence
	}
	var validlicence Licence
	json.Unmarshal(body, &validlicence)
	log.Info("licence valid")
	return validlicence
}
