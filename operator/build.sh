#!/bin/bash

dnf -y install go-toolset
dnf -y install podman-docker make
# glibc-common glibc-locale-source
# export LC_ALL=
# localedef -i en_US -f UTF-8 en_US.UTF-8

export ARCH=$(case $(uname -m) in x86_64) echo -n amd64 ;; aarch64) echo -n arm64 ;; *) echo -n $(uname -m) ;; esac)
export OS=$(uname | awk '{print tolower($0)}')
export OPERATOR_SDK_DL_URL=https://github.com/operator-framework/operator-sdk/releases/download/v1.25.0
curl -LO ${OPERATOR_SDK_DL_URL}/operator-sdk_${OS}_${ARCH}
chmod +x operator-sdk_${OS}_${ARCH} && mv operator-sdk_${OS}_${ARCH} /bin/operator-sdk

export HELM_OPERATOR_SDK_DL_URL=https://github.com/operator-framework/operator-sdk/releases/download/v1.25.0
curl -LO ${HELM_OPERATOR_SDK_DL_URL}/helm-operator_${OS}_${ARCH}
chmod +x helm-operator_${OS}_${ARCH} && mv helm-operator_${OS}_${ARCH} /bin/helm-operator

# @TODO Openshift 3.11 will not work

# operator-sdk init --plugins=hybrid.helm.sdk.operatorframework.io/v1-alpha --domain="xrow.com" --project-name="symfony" --project-version="3" --license="none" --owner="xrow GmbH" --repo=gitlab.com/xrow-public/symfony-operator
# operator-sdk create api --plugins helm.sdk.operatorframework.io/v1 --group="framework" --version v1 --kind Symfony
# export HELM_OPERATOR="/bin/helm-operator"

make test
make docker-build
make docker-push