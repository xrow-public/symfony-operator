# Symfony (Demo) by XROW GmbH

An opinionated build of Symfony providing a basis for projects by XROW GmbH.

It includes all standard bundles, and can be built using [s2i](https://github.com/openshift/source-to-image).

## Bootstrap a new repository

Login via ssh

```bash
git init
git checkout -b feature-01
git add .
git commit -m "initial commit"
git remote add origin git@gitlab.com:xrowgmbh/external/XXX/XXX-de.git
git push -u origin feature-01
```
