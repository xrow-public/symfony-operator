#!/bin/bash

export SYMFONY_VERSION="${SYMFONY_VERSION:-6.0.*}"
export S2I_IMAGE="${PHP_IMAGE:-registry.gitlab.com/xrow-public/repository/php:latest}"

rm -Rf src
mkdir -p src
chmod 0777 -R src
podman run -it --entrypoint="/bin/bash" \
  -e "TZ=Europe/Berlin" \
  -e "APP_ENV=prod" \
  -e "APP_DEBUG=0" \
  -e "DOCUMENTROOT=/public" \
  -v ./src:/tmp/src:Z \
  ${S2I_IMAGE} << EOF
composer create-project symfony/skeleton:${SYMFONY_VERSION} /tmp/src
cd /tmp/src
composer require -n webapp
exit
EOF
cp -R -f --preserve=all root/* src
cp -R -f --preserve=all root/.[^.]* src
podman build \
  --build-arg S2I_IMAGE=${S2I_IMAGE} \
  -t localhost/framework:latest \
  -t registry.gitlab.com/xrow-public/symfony-operator/framework:latest \
  -f Dockerfile \
  ./src

podman login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
podman push registry.gitlab.com/xrow-public/symfony-operator/framework:latest