# Documentation

```bash
export PATH=$PATH:$PWD/node_modules/.bin
yarn install
#@TODO missing console encore?
yarn run encore dev-server --live-reload --stats --server-type https --port 4300 \
 --config-name app --host 0.0.0.0 --allowed-hosts all \
 --public https://192.168.222.2:4300 \
 --server-options-cert /etc/pki/tls/certs/localhost.crt \
 --server-options-key /etc/pki/tls/private/localhost.key
```
