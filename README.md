# Symfony Operator

[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/symfony-operator)](https://artifacthub.io/packages/search?repo=symfony-operator)


The Kubernetes Symfony Operator creates an environment for any symfony application.

It is triggered by placing a _symfony_ CRD (Custom Resource Definition) of type "symfony" in a given namespace.

http://symfony-operator.xrow.de/